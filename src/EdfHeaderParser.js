const Edf = require('./Edf');
const Signal = require('./Signal');

const SAMPLE_BYTE_LENGTH = 2;

const headerSpec = [
    {"name": "version", "length": 8},
    {"name": "patientId", "length": 80},
    {"name": "recordingId", "length": 80},
    {"name": "startDate", "length": 8},
    {"name": "startTime", "length": 8},
    {"name": "numberOfBytes", "length": 8},
    {"name": "reserved", "length": 44},
    {"name": "numDataRecords", "length": 8},
    {"name": "durationOfDataRecord", "length": 8},
    {"name": "numSignalsInDataRecord", "length": 4}
];

const signalSpec = [
    {"name": "label", "length": 16},
    {"name": "transducerType", "length": 80},
    {"name": "physicalDimensions", "length": 8},
    {"name": "physicalMin", "length": 8},
    {"name": "physicalMax", "length": 8},
    {"name": "digitalMin", "length": 8},
    {"name": "digitalMax", "length": 8},
    {"name": "prefiltering", "length": 80},
    {"name": "numSamplesInDataRecord", "length": 8}
];


class EdfHeaderParser {

    constructor() {
        this.timezone = 'UTC';
    }

    computeAdditionalEdfParams() {
        this.edf.numSamplesInDataRecord = 0;
        this.edf.bytesInDataRecord = 0;
        this.edf.getSignals().forEach(signal => {
            this.edf.numSamplesInDataRecord += signal.numSamplesInDataRecord;
            this.edf.bytesInDataRecord += signal.bytesInDataRecord;
        })
    }

    parseEdfHeaders(edfHeader) {
        let start = 0;
        headerSpec.forEach(spec => {
            const end = start + spec.length;
            this.edf[spec.name] = edfHeader.slice(start, end).toString().trim();
            start = end;
        });
    }

    computeAdditionalSignalParams(signal) {
        signal.sampleDuration = this.edf.durationOfDataRecord / signal.numSamplesInDataRecord;
        signal.sampleRate = signal.numSamplesInDataRecord / this.edf.durationOfDataRecord;
        signal.bytesInDataRecord = signal.numSamplesInDataRecord * SAMPLE_BYTE_LENGTH;
    }

    parseSignalHeaders(signalHeaders) {
        const signalsCount = this.edf.numSignalsInDataRecord;
        for (let i = 0; i < signalsCount; i++) {
            this.edf.getSignals().push(new Signal());
        }
        let start = 0;
        signalSpec.forEach(
            spec => this.edf.getSignals().forEach(signal => {
                const end = start + spec.length;
                signal[spec.name] = signalHeaders.slice(start, end).toString().trim();
                start = end;
            })
        );
        this.edf.getSignals().forEach(this.computeAdditionalSignalParams.bind(this));
        this.computeAdditionalEdfParams();
    }

    parse(readable) {
        if (!this.edf) {
            let edfHeader;
            let signalHeaders;
            this.edf = new Edf(this.timezone);
            return new Promise((resolve, reject) => {
                readable.on('readable', () => {
                    edfHeader = readable.read(256);
                    this.parseEdfHeaders(edfHeader);
                    signalHeaders = readable.read(256 * this.edf.numSignalsInDataRecord);
                    this.parseSignalHeaders(signalHeaders);
                    readable.removeAllListeners('readable');
                    resolve(this.edf);
                });
            });
        }
    }
}

module.exports = EdfHeaderParser;
