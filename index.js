const EdfParser = require('./src/EdfParser');
const EdfHeaderParser = require('./src/EdfHeaderParser');
const EdfFileParser = require('./src/EdfFileParser');
const EdfZipParser = require('./src/EdfZipParser');

module.exports = {
    EdfParser: EdfParser,
    EdfFileParser: EdfFileParser,
    EdfHeaderParser: EdfHeaderParser,
    EdfZipParser: EdfZipParser
};
